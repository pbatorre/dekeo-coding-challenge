class Pack
  attr_reader :capacity, :price

  def initialize(capacity, price)
    @capacity = capacity
    @price = price
  end

  def <=>(other)
    capacity <=> other.capacity
  end
end
