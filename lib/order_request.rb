class OrderRequest
  InvalidQuantityError = Class.new(StandardError)

  attr_reader :product_code, :quantity

  def initialize(product_code, quantity)
    @product_code = product_code
    @quantity = quantity
  end

  def self.parse(product_code, quantity)
    begin
      formatted_input = Integer(quantity)

      if formatted_input.negative? || formatted_input != Float(quantity)
        raise InvalidQuantityError
      end

      OrderRequest.new(product_code, formatted_input)
    rescue => e
      error_message = "Quantity for #{product_code} must be a positive whole number"

      raise InvalidQuantityError, error_message
    end
  end
end
