require_relative './number_helper'

class Solution
  def initialize(product, solution)
    @product = product
    @solution = solution
  end

  def render
    total_amount = 0
    total_quantities = 0
    line_items = []

    solution.each do |item|
      pack = item[:pack]
      total_amount += (item[:quantity] * pack.price)
      total_quantities += item[:quantity] * pack.capacity
      price_per_pack = NumberHelper.as_usd(pack.price)

      line_items << "\t\t#{item[:quantity]} x #{pack.capacity} #{price_per_pack}"
    end

    formatted_total = NumberHelper.as_usd(total_amount)
    puts "#{total_quantities}\t#{product.code}\t#{formatted_total}"

    line_items.each { |line_item| puts line_item }
  end

  private

  attr_reader :product, :solution
end
