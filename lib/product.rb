require_relative './packer'

class Product
  UnrecognizedObjectError = Class.new(StandardError)
  PackAlreadyAddedError = Class.new(StandardError)

  attr_reader :code, :packs

  def initialize(code)
    @code = code
    @packs = []
  end

  def add_pack(pack)
    raise UnrecognizedObjectError unless pack.instance_of?(Pack)
    raise PackAlreadyAddedError if find_pack(pack)

    packs << pack
  end

  def fulfill(quantity)
    capacities = packs.map(&:capacity)
    packer = Packer.new(capacities)

    result = packer.fulfill(quantity)
    collate_fulfillment(result)
  end

  def <=>(other)
    code <=> other.code
  end

  private

  def collate_fulfillment(result)
    result.group_by(&:itself).map do |capacity, instances|
      {
        quantity: instances.count,
        pack: find_pack(capacity),
      }
    end
  end

  def find_pack(capacity)
    packs.find do |pack|
      pack.capacity == capacity
    end
  end
end
