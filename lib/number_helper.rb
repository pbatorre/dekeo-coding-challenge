module NumberHelper
  def self.as_usd(amount)
    "$#{amount.round(2)}"
  end
end
