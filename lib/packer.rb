class Packer
  UnableToFulfillOrderError = Class.new(StandardError)

  def initialize(quantities)
    @quantities = quantities
    @solutions = []
  end

  def fulfill(order)
    solutions.clear

    find_optimal_allocation(order, quantities)

    solutions.min_by(&:length) || (raise UnableToFulfillOrderError)
  end

  private

  attr_reader :quantities, :solutions

  def find_optimal_allocation(remaining_order, quantities, current_solution = [])
    return solutions << current_solution if remaining_order.zero?

    if unfulfillable?(quantities, remaining_order)
      return current_solution.pop && false
    end

    current_largest_capacity = quantities.first
    for_packing = remaining_order - quantities.first
    updated_solution = current_solution + [current_largest_capacity]

    find_optimal_allocation(for_packing, quantities, updated_solution)
    find_optimal_allocation(remaining_order, quantities.drop(1), current_solution)
  end

  def unfulfillable?(quantities, remaining_order)
    quantities.empty? || remaining_order.negative?
  end
end
