class Inventory
  ProductAlreadyAddedError = Class.new(StandardError)

  attr_reader :products

  def initialize
    @products = []
  end

  def add_product(product)
    raise ProductAlreadyAddedError if find_product(product)

    products << product
  end

  def find_product(code)
    products.find do |product|
      product.code == code
    end
  end
end
