require 'optparse'

require_relative '../lib/order_request'
require_relative '../lib/inventory'
require_relative '../lib/product'
require_relative '../lib/pack'
require_relative '../lib/solution'

vegemite_scroll = Product.new('VS5')
blueberry_muffin = Product.new('MB11')
croissant = Product.new('CF')

vegemite_scroll.add_pack(Pack.new(3, 6.99))
vegemite_scroll.add_pack(Pack.new(5, 8.99))

blueberry_muffin.add_pack(Pack.new(2, 9.95))
blueberry_muffin.add_pack(Pack.new(5, 16.95))
blueberry_muffin.add_pack(Pack.new(8, 24.95))

croissant.add_pack(Pack.new(3, 5.95))
croissant.add_pack(Pack.new(5, 9.95))
croissant.add_pack(Pack.new(9, 16.99))

inventory = Inventory.new

inventory.add_product(vegemite_scroll)
inventory.add_product(blueberry_muffin)
inventory.add_product(croissant)

options = {}

OptionParser.new do |opt|
  inventory.products.map do |product|
    opt.on("--#{product.code} quantity") { |o| options[product.code] = o }
  end
end.parse!

begin
  options.each do |product_code, quantity|
    order_request = OrderRequest.parse(product_code, quantity)

    product = inventory.find_product(product_code)
    result = product.fulfill(order_request.quantity)

    Solution.new(product, result).render
  end
rescue OrderRequest::InvalidQuantityError => e
  puts e
end
