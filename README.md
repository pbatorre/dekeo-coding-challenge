# Problem

See problem description [here](./problem-description.pdf).

# Setup

Run `bundle install` to install the dependencies

# Testing

To test the sample input in the [problem description](./problem-description.pdf), run the following command:

`ruby bin/runner.rb --VS5=10 --MB11=14 --CF=13`
