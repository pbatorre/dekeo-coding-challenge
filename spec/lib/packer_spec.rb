require 'packer'

describe Packer do
  let(:packs) { [4, 3, 2] }
  let(:packer) do
    Packer.new(packs)
  end

  describe '#find' do
    context 'when available packs cannot fulfill the exact order' do
      it 'raises an error' do
        expect { packer.fulfill(1) }
          .to raise_exception(Packer::UnableToFulfillOrder)
      end
    end

    it 'returns a packaging that has the minimum number of packs' do
      expect(packer.fulfill(21))
        .to match_array([4, 4, 4, 4, 3, 2])
    end
  end
end
