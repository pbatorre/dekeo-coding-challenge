require 'product'
require 'pack'

describe Product do
  let(:product) do
    Product.new("PROD1")
  end

  describe '#add_pack' do
    context 'when instance of pack is unique' do
      let(:pack) do
        Pack.new(2, 2)
      end

      it 'adds the pack to the product' do
        expect(product.add_pack(pack))
          .to eql([pack])
      end
    end

    context 'when param is not a pack instance' do
      it 'raises InvalidPack' do
        expect {
          product.add_pack(1)
        }.to raise_exception(Product::InvalidPack)
      end
    end

    context 'when pack with the same quantity already exists' do
      let(:pack) do
        Pack.new(2, 2)
      end

      before do
        product.add_pack(pack)
      end

      it 'raises DuplicatePack' do
        duplicate_pack = Pack.new(2, 5)

        expect {
          product.add_pack(duplicate_pack)
        }.to raise_exception(Product::DuplicatePack)
      end
    end
  end

  describe '#fulfill' do
    let(:pack_1) do
      Pack.new(4, 5.99)
    end
    let(:pack_2) do
      Pack.new(3, 3.50)
    end
    let(:pack_3) do
      Pack.new(2, 2.50)
    end

    before do
      product.add_pack(pack_1)
      product.add_pack(pack_2)
      product.add_pack(pack_3)
    end

    context 'when order can be fulfilled' do
      it 'returns the quantities needed to fulfill the order' do
        result = product.fulfill(17)

        expect(result).to match_array(
          [
            {
              quantity: 3,
              pack: pack_1,
            },
            {
              quantity: 1,
              pack: pack_2,
            },
            {
              quantity: 1,
              pack: pack_3,
            },
          ]
        )
      end
    end

    context 'when order cannot be fulfilled' do
      it 'raises an error' do
        expect {
          product.fulfill(1)
        }.to raise_error(Packer::UnableToFulfillOrder)
      end
    end
  end
end
