require 'inventory'
require 'product'

describe Inventory do
  let(:inventory) { Inventory.new }

  describe '#add_product' do
    context 'valid' do
      let(:product) { Product.new('FOO') }

      it 'adds the product to the inventory' do
        expect(inventory.add_product(product))
          .to eql([product])
      end
    end

    context 'when product has already been added to the inventory' do
      let(:product) { Product.new('FOO') }

      before do
        inventory.add_product(product)
      end

      it 'raises ProductAlreadyExists' do
        expect { inventory.add_product(product) }
          .to raise_exception(Inventory::DuplicateProduct)
      end
    end
  end
end
