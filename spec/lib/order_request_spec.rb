require 'order_request'

describe OrderRequest do
  describe '.parse' do
    context 'when quantity is a positive whole number' do
      it 'returns an OrderRequest instance' do
        result = OrderRequest.parse('PROD1', '5')

        expect(result.product_code).to eq 'PROD1'
        expect(result.quantity).to eq 5
      end
    end

    context 'when quantity is not a positive whole number' do
      it 'raises InvalidQuantity' do
        expect {
          OrderRequest.parse('PROD1', 1.2)
        }.to raise_exception(OrderRequest::InvalidQuantity)
      end
    end
  end
end
